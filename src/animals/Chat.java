package animals;

public class Chat extends Animal{
	
	
	public Chat(String nom, String race, String couleur) {
		super(nom, race, couleur);		
	}	


	@Override
	public String toString() {
		return "chat [getNom()=" + getNom() + ", getRace()=" + getRace() + ", getCouleur()=" + getCouleur()
				+ ", toString()=" + super.toString();
	}

	@Override
	public void crier() {
		System.out.println("Meowwwwww");
		
	}

}
