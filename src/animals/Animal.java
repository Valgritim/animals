package animals;

public class Animal implements IAnimals{

	private String nom;
	private String race;
	private String couleur;
	
	public Animal(String nom, String race, String couleur) {
		super();
		this.nom = nom;
		this.race = race;
		this.couleur = couleur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	@Override
	public String toString() {
		return "animal [nom=" + nom + ", race=" + race + ", couleur=" + couleur + "]";
	}

	@Override
	public void crier() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
}
